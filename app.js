const express = require('express');
var app = express();
const bodyParser = require('body-parser');
const showRouter = require('./showRouter');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json())
app.use('/show', showRouter);

app.listen(3000);
console.log('Server is running at port: 3000');



