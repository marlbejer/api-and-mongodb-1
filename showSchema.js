const mongoose = require('mongoose');

//create schema
const showSchema = mongoose.Schema({
    title: String,
    type: String,
    genre: String,
    year: Number,
    rating: Number,
    status: String
});

const Show = module.exports = mongoose.model('Show', showSchema);

module.exports.getShows = (callback, limit) => {
    Show.find(callback).limit(limit);
}

module.exports.postShow = (data, callback) => {
    Show.create(data, callback);
}

module.exports.putShow = (id, show, options, callback) => {
    let query = {_id: id};
    let update = {
        title: show.title,
        type: show.type,
        genre: show.genre,
        year: show.year,
        rating: show.rating,
        status: show.status
    }
    Show.findOneAndUpdate(query, update, options, callback);
}

module.exports.deleteShow = (id, callback) => {
    let query = {_id: id};
    Show.remove(query, callback);
}