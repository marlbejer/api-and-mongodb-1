const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

Show = require('./showSchema');

mongoose.connect('mongodb://localhost:27017/demo', { useNewUrlParser: true}, (err, db) => {
    if(err) throw err;
    console.log('Connected to Database');
});
const db = mongoose.connection;

router.get('/', (req, res) => {
    Show.getShows((err, shows) => {
        if(err) throw err;
        res.json(shows);
    });
});

router.post('/', (req, res) => {
    let show = req.body;
    Show.postShow(show, (err, data) => {
        if(err) throw err;
        res.json(data);
    });
});

router.put('/:_id', (req, res) => {
    let id = req.params._id;
    let show = req.body;
    Show.putShow(id, show, {}, (err, data) => {
        if(err) throw err;       
    });
    Show.find({_id: id}, (errx, data) => {
        if(errx) throw errx;
        res.json(data);
    });
});

router.delete('/:_id', (req, res) => { 
    let id = req.params._id;
    Show.deleteShow(id, (err, data) => {
        if(err) throw err;
        res.json(data);
    });
});

module.exports = router;
